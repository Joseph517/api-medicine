from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(Doctor)
admin.site.register(Patient)
admin.site.register(Appointment)
admin.site.register(Schedule)
admin.site.register(Specialty)
admin.site.register(DoctorSpecialty)