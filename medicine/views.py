from django.shortcuts import render

# Create your views here.
from .models import *
from rest_framework import viewsets
from .serializers import DoctorSerializer, PatientSerializer, AppointmentSerializer, ScheduleSerializer, \
    SpecialtySerializer, DoctorSpecialtySerializer


class DoctorViewSet(viewsets.ModelViewSet):
    queryset = Doctor.objects.all().order_by('name')
    serializer_class = DoctorSerializer


class PatientViewSet(viewsets.ModelViewSet):
    queryset = Patient.objects.all().order_by('name')
    serializer_class = PatientSerializer


class AppointmentViewSet(viewsets.ModelViewSet):
    queryset = Appointment.objects.all().order_by('idDoctor')
    serializer_class = AppointmentSerializer


class ScheduleViewSet(viewsets.ModelViewSet):
    queryset = Schedule.objects.all().order_by('idDoctor')
    serializer_class = ScheduleSerializer


class SpecialtyViewSet(viewsets.ModelViewSet):
    queryset = Specialty.objects.all().order_by('name')
    serializer_class = SpecialtySerializer


class DoctorSpecialtyViewSet(viewsets.ModelViewSet):
    queryset = DoctorSpecialty.objects.all().order_by('idDoctor')
    serializer_class = DoctorSpecialtySerializer
