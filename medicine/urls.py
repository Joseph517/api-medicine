from django.urls import path, include
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'doctor', views.DoctorViewSet)
router.register(r'patient', views.PatientViewSet)
router.register(r'appointment', views.AppointmentViewSet)
router.register(r'shedule', views.ScheduleViewSet)
router.register(r'specialty', views.SpecialtyViewSet)
router.register(r'doctor-specialty', views.DoctorSpecialtyViewSet)

app_name = 'medicine'

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
