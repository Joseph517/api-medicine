from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Doctor(models.Model):
    name = models.CharField(max_length=50)
    lastName = models.CharField(max_length=50)
    dni = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    correo = models.EmailField()
    phone = models.CharField(max_length=50)
    sex = models.CharField(max_length=50)
    numTuition = models.CharField(max_length=50)
    birthDate = models.DateField()
    registerDate = models.DateField()
    userRegister = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)
    userModification = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)
    active = models.BooleanField()

    def __str__(self):
        return self.name


class Patient(models.Model):
    name = models.CharField(max_length=50)
    lastName = models.CharField(max_length=50)
    dni = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    correo = models.EmailField()
    phone = models.CharField(max_length=50)
    sex = models.CharField(max_length=50)
    birthDate = models.DateField()
    registerDate = models.DateField()
    userRegister = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)
    userModification = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)
    active = models.BooleanField()

    def __str__(self):
        return self.name


class Appointment(models.Model):
    idDoctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    idPatient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    attentionDate = models.DateField()
    startAttentionDate = models.DateTimeField()
    endAttentionDate = models.DateTimeField()
    status = models.BooleanField()
    registerDate = models.DateTimeField()
    userRegister = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)
    dateModification = models.DateTimeField()
    userModification = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)


class Schedule(models.Model):
    idDoctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    dateAttention = models.DateTimeField()
    startDateAttention = models.DateTimeField()
    endDateAttention = models.DateTimeField()
    active = models.BooleanField()
    dateRegister = models.DateTimeField()
    userRegister = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)
    dateModification = models.DateTimeField()
    userModification = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)


class Specialty(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    dateRegister = models.DateField()
    dateModification = models.DateTimeField()
    userRegister = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)
    userModification = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)
    active = models.BooleanField()


class DoctorSpecialty(models.Model):
    idDoctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    idSpecialty = models.ForeignKey(Specialty, on_delete=models.CASCADE)
    dateRegister = models.DateTimeField()
    dateModification = models.DateField()
    userRegister = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)
    userModification = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)
    active = models.BooleanField()
